import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { CredenciamentoPage } from '../pages/credenciamento/credenciamento';
import { SelecionaAreaPage } from '../pages/seleciona-area/seleciona-area';
import { RegistroPage } from '../pages/registro/registro';
import { SelecionaVotaOPage } from '../pages/seleciona-vota-o/seleciona-vota-o';
import { VotaOPage } from '../pages/vota-o/vota-o';
import { ConfigurarPage } from '../pages/configurar/configurar';


import { PPAConsultasPBlicasPage } from '../pages/p-paconsultas-pblicas/p-paconsultas-pblicas';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) navCtrl: Nav;
    rootPage:any = PPAConsultasPBlicasPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
  goToCredenciamento(params){
    if (!params) params = {};
    this.navCtrl.setRoot(CredenciamentoPage);
  }goToSelecionaArea(params){
    if (!params) params = {};
    this.navCtrl.setRoot(SelecionaAreaPage);
  }goToRegistro(params){
    if (!params) params = {};
    this.navCtrl.push(RegistroPage, {
        'area': params.area
    });
  }goToSelecionaVotaO(params){
    if (!params) params = {};
    this.navCtrl.setRoot(SelecionaVotaOPage);
  }goToVotaO(params){
    if (!params) params = {};
    this.navCtrl.push(VotaOPage, {
        'votation': params.votation
    });
  }goToConfigurar(params){
    if (!params) params = {};
    this.navCtrl.setRoot(ConfigurarPage);
  }
}
