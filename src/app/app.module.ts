import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { PPAConsultasPBlicasPage } from '../pages/p-paconsultas-pblicas/p-paconsultas-pblicas';
import { SelecionaVotaOPage } from '../pages/seleciona-vota-o/seleciona-vota-o';
import { VotaOPage } from '../pages/vota-o/vota-o';
import { CredenciamentoPage } from '../pages/credenciamento/credenciamento';
import { SelecionaAreaPage } from '../pages/seleciona-area/seleciona-area';
import { CriarReaPage } from '../pages/criar-rea/criar-rea';
import { RegistroPage } from '../pages/registro/registro';
import { DevoluOPage } from '../pages/devolu-o/devolu-o';
import { ConfigurarPage } from '../pages/configurar/configurar';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    MyApp,
    PPAConsultasPBlicasPage,
    SelecionaVotaOPage,
    VotaOPage,
    CredenciamentoPage,
    SelecionaAreaPage,
    CriarReaPage,
    RegistroPage,
    DevoluOPage,
    ConfigurarPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PPAConsultasPBlicasPage,
    SelecionaVotaOPage,
    VotaOPage,
    CredenciamentoPage,
    SelecionaAreaPage,
    CriarReaPage,
    RegistroPage,
    DevoluOPage,
    ConfigurarPage
  ],
  providers: [
    StatusBar,
    SplashScreen,InAppBrowser,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
