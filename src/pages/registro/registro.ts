import { Component } from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'page-registro',
  templateUrl: 'registro.html'
})
export class RegistroPage {
  area: any;
  tag: number;
  tag_host: any;
  show_card: boolean=false;
  result_data: any;

  constructor(
      public navCtrl: NavController,
      public params: NavParams,
      public http: HttpClient
      ) {
    this.area = params.get('area');
    this.tag_host = window.localStorage.getItem("tag_host") || "";
    this.result_data= {status: false};
  }

  register(){
      this.http.get(this.tag_host + "/consultas_publicas/registro?area=" + this.area + "&tag="+ this.tag).subscribe(data => {
          console.log('my data: ', data);
          this.result_data= data;
          this.show_card = true;
          this.tag = null;
      });
  };



}
