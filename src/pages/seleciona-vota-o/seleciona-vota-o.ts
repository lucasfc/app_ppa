import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { VotaOPage } from '../vota-o/vota-o';

@Component({
  selector: 'page-seleciona-vota-o',
  templateUrl: 'seleciona-vota-o.html'
})
export class SelecionaVotaOPage {

  votation: any;
  event_id: any;
  theme_id: any;

  constructor(public navCtrl: NavController) {
      this.event_id = window.localStorage.getItem("event_id") || "";
      this.theme_id = window.localStorage.getItem("theme_id") || "";
      this.votation = "E" + this.event_id + "T" + this.theme_id
  }
  goToVotaO(params){
    if (!params) params = {};
    this.navCtrl.push(VotaOPage, {
        'votation': this.votation
    });
  }
}
