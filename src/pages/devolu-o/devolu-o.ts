import { Component } from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'page-devolu-o',
  templateUrl: 'devolu-o.html'
})
export class DevoluOPage {

    tag: number;
    tag_host: any;
    show_card: boolean=false;
    result_data: any;

    constructor(
        public navCtrl: NavController,
        public params: NavParams,
        public http: HttpClient
    ) {
        this.tag_host = window.localStorage.getItem("tag_host") || "";
        this.result_data= {status: false};
  }

    register(){
        this.http.get(this.tag_host + "/consultas_publicas/devolucao?tag="+ this.tag).subscribe(data => {
            console.log('my data: ', data);
            this.result_data= data;
            this.show_card = true;
            this.tag = null;
        });
    };


}
