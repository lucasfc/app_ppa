import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'page-credenciamento',
  templateUrl: 'credenciamento.html'
})
export class CredenciamentoPage {

  credenciamento_page: any;
  credenciamento_painel: any;
  credenciamento_cidades: any;
  credenciamento_categoria: any;
  credenciamento_buscar: any;
  ppa_host: any;
  event_id: any;
  theme_id: any;

  constructor(public navCtrl: NavController, private iab: InAppBrowser) {
    this.ppa_host = window.localStorage.getItem("ppa_host") || "";
    this.event_id = window.localStorage.getItem("event_id") || "";
    this.theme_id = window.localStorage.getItem("theme_id") || "";
    this.credenciamento_page = this.ppa_host + "/home/credenciamento";
    this.credenciamento_buscar = this.ppa_host + "/people";
    this.credenciamento_painel = this.ppa_host + "/home/painel_credenciamento?&event_id="+this.event_id+"&theme_id=" + this.theme_id;
    this.credenciamento_cidades = this.ppa_host + "/home/painel_cidades?&event_id="+this.event_id+"&theme_id=" + this.theme_id;
    this.credenciamento_categoria = this.ppa_host + "/home/painel_categorias?&event_id="+this.event_id+"&theme_id=" + this.theme_id;
  }

  open_page(url){
      this.iab.create(url);
  }
  
}
