import { Component, ViewChild} from '@angular/core';
import {NavController, NavParams, AlertController, } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';



@Component({
  selector: 'page-vota-o',
  templateUrl: 'vota-o.html'
})

export class VotaOPage {

  show_votation: boolean=false;
  show_alert: boolean=false;
  show_card: boolean=false;
  tag: number;
  votation: any;
  votation_data: any;
  replies: any=[];
  tag_host: any;
  result_data: any= {status: false, mensagem: "Pesquisando..."};
  result_vote: any= {status: false};

  constructor(
      public navCtrl: NavController,
      public params: NavParams,
      public alertCtrl: AlertController,
      public http: HttpClient
  ) {
      this.votation_data= {code: null};
      this.votation = params.get('votation');
      this.tag_host = window.localStorage.getItem("tag_host") || "";
      this.http.get(this.tag_host + "/consultas_publicas/votacao?votation=" + this.votation).subscribe(data => {
          console.log('my data: ', data);
          this.result_data = data;
          this.votation_data= this.result_data.votation;
          this.replies = this.result_data.replies;
      });
  }

  enviar_voto(id){
    this.show_votation = false;
      this.http.get(this.tag_host + "/consultas_publicas/receber_voto?vote=" + id + "&tag="+  this.tag).subscribe(data => {
          console.log('my data: ', data);
          this.result_vote = data;
          this.show_card = true;
          this.show_votation= false;
          this.tag= null;
      });
  }

  showAlert(titulo,msg) {
      let alert = this.alertCtrl.create({
          title: titulo,
          subTitle: msg,
          buttons: ['OK']
      });
      alert.present();
  }

    moveFocus(nextElement) {
        nextElement.setFocus();
    }

}
