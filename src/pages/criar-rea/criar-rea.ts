import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import {RegistroPage} from "../registro/registro";


@Component({
  selector: 'page-criar-rea',
  templateUrl: 'criar-rea.html'
})
export class CriarReaPage {
  tag_host: any;
  area: any={
    name: "",
    description: "",
    limit: "",
    exit_register: false,
    webhook: "",
    theme_id: "",
    event_id: "",
    code: ""
  };

  result: any;

  constructor(public navCtrl: NavController, public http: HttpClient) {
      this.tag_host = window.localStorage.getItem("tag_host") || "";
  }

  registrar(){
      this.http.get(this.tag_host + "/consultas_publicas/criar_area", {params:{area: JSON.stringify(this.area)}}).subscribe(data => {
        this.result= data;
        this.navCtrl.push(RegistroPage, {
              'area': this.result.data
          });
      });
  }

}
