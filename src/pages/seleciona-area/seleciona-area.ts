import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RegistroPage } from '../registro/registro';

@Component({
  selector: 'page-seleciona-area',
  templateUrl: 'seleciona-area.html'
})
export class SelecionaAreaPage {

  area: any;
  event_id: any;
  theme_id: any;

  constructor(public navCtrl: NavController) {
      this.event_id = window.localStorage.getItem("event_id") || "";
      this.theme_id = window.localStorage.getItem("theme_id") || "";
      this.area = "E" + this.event_id + "T" + this.theme_id
  }
  goToRegistro(params){
    if (!params) params = {};
    this.navCtrl.push(RegistroPage, {
        'area': this.area
    });
  }
}
