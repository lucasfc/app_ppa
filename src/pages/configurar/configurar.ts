import { Component } from '@angular/core';
import {AlertController, NavController} from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import {CriarReaPage} from "../criar-rea/criar-rea";

@Component({
  selector: 'page-configurar',
  templateUrl: 'configurar.html'
})
export class ConfigurarPage {

  ppa_host: any;
  tag_host: any;
  event_id: any;
  theme_id: any;

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public http: HttpClient
  ) {
    this.ppa_host = window.localStorage.getItem("ppa_host") || "";
    this.tag_host = window.localStorage.getItem("tag_host") || "";
    this.event_id = window.localStorage.getItem("event_id") || "";
    this.theme_id = window.localStorage.getItem("theme_id") || "";
  }


  salvar() {
    window.localStorage.setItem("ppa_host", this.ppa_host);
    window.localStorage.setItem("tag_host", this.tag_host);
    window.localStorage.setItem("event_id", this.event_id);
    window.localStorage.setItem("theme_id", this.theme_id);

    this.http.get(this.tag_host + "/consultas_publicas/configuration?namespace=ppa_host&value=" + this.ppa_host).subscribe(data => {
      console.log('my data: ', data);
    });
    this.showAlert("Dados Enviados", "Os dados de configuração foram enviados com sucesso")
  }

  showAlert(titulo,msg) {
    let alert = this.alertCtrl.create({
      title: titulo,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

    goToCriarArea(){
        this.navCtrl.push(CriarReaPage);
    }
}
