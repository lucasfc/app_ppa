import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CredenciamentoPage } from '../credenciamento/credenciamento';
import { SelecionaAreaPage } from '../seleciona-area/seleciona-area';
import { RegistroPage } from '../registro/registro';
import { SelecionaVotaOPage } from '../seleciona-vota-o/seleciona-vota-o';
import { VotaOPage } from '../vota-o/vota-o';
import { DevoluOPage } from '../devolu-o/devolu-o';

@Component({
  selector: 'page-p-paconsultas-pblicas',
  templateUrl: 'p-paconsultas-pblicas.html'
})
export class PPAConsultasPBlicasPage {

  constructor(public navCtrl: NavController) {
  }
  goToCredenciamento(params){
    if (!params) params = {};
    this.navCtrl.push(CredenciamentoPage);
  }goToSelecionaArea(params){
    if (!params) params = {};
    this.navCtrl.push(SelecionaAreaPage);
  }goToRegistro(params){
    if (!params) params = {};
    this.navCtrl.push(RegistroPage, {
        'area': params.area
    });
  }goToSelecionaVotaO(params){
    if (!params) params = {};
    this.navCtrl.push(SelecionaVotaOPage);
  }goToVotaO(params){
    if (!params) params = {};
    this.navCtrl.push(VotaOPage, {
        'votation': params.votation
    });
  }goToDevoluO(params){
    if (!params) params = {};
    this.navCtrl.push(DevoluOPage, {
        'area': params.area
    });
  }
}
